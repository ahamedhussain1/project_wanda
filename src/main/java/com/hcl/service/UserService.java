package com.hcl.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.entity.User;
import com.hcl.repository.UserRepository;

@Service
public class UserService {
	
	@Autowired
	UserRepository userRepo;
	
	
	// To Register as User
	public User createUser( User user) {
		return userRepo.save(user);
	}
	
	// To Login as User
	public boolean loginCheck(String email, String password) {
		List <User> users = userRepo.findAll();
		boolean isLogged = false;
		for(User user:users) {
			if(user.getEmail().compareTo(email) == 0 && user.getPassword().compareTo(password) == 0) {
				isLogged = true;
			}
		}
		return isLogged;
	}


}
