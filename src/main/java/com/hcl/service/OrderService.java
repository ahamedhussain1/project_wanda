package com.hcl.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.entity.Order;
import com.hcl.repository.OrderRepository;

@Service
public class OrderService {
	
	@Autowired
	OrderRepository orderRepo;
	
	
	// Create
	public Order createOrder(Order order) {
		return orderRepo.save(order);
	}
	
	// Read or View
	public List<Order> ViewAllOrders() {
		return orderRepo.findAll();
	}
	
	// Delete
	public void deleteOrder(Integer id) {
		orderRepo.deleteById(id);
	}
	
	// Find By Order by Userid
	public List<Order> getByuserid(Integer userid) {
		return orderRepo.findByuserid(userid);
	}
	
}
