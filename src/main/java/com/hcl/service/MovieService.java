package com.hcl.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.hcl.entity.Movie;
import com.hcl.repository.MovieRepository;

@Service
public class MovieService {
	
	@Autowired
	MovieRepository movieRepo;
	
	// Create
	public Movie createMovie(Movie movie) {
		return movieRepo.save(movie);
	}

	// Read or View
	public List<Movie> viewAllMovies() {
		return movieRepo.findAll();
	}

	// Update
	public Movie updateMovie(Integer id,Movie movie) {
		Movie m = movieRepo.findById(id).get();
		m.setMoviename(movie.getMoviename());
		m.setGenre(movie.getGenre());
		m.setLanguage(movie.getLanguage());
		m.setReleasetype(movie.getReleasetype());
		m.setPrice(movie.getPrice());
		return movieRepo.save(m);
	}	
		
	// Delete
	public void deleteMovie(Integer id) {
		movieRepo.deleteById(id);
	}
	
	// Find By Genre
	public List<Movie> getBygenre(String genre) {
		return movieRepo.findBygenre(genre);	
	}
	
	//Find by Language
	
	public List<Movie> getBylanguage(String language) {
		return movieRepo.findBylanguage(language);
	}
	
	//Find by Release Type
	public List<Movie> getByreleasetype(String releasetype) {
		return movieRepo.findByreleasetype(releasetype);
	}
	
	//Find by Movie Name
	public List<Movie> getBymoviename(String moviename) {
		return movieRepo.findBymoviename(moviename);
	}
	
	// Find by movieId
	public Movie getById(int id) {
		return movieRepo.findById(id).get();
	}

}
