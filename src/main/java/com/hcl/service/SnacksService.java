package com.hcl.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.entity.Snacks;
import com.hcl.repository.SnacksRepository;

@Service
public class SnacksService {
	
	@Autowired
	SnacksRepository snacksRepo;
	
	// Create
	public Snacks createSnacks(Snacks snacks) {
		return snacksRepo.save(snacks);
	}
	
	// Read or View
	public List<Snacks> viewAllSnackss() {
		return snacksRepo.findAll();
	}
	
	// Update 
	public Snacks updateSnacks(Integer id,Snacks snacks) {
		Snacks s = snacksRepo.findById(id).get();
		s.setSnacksname(snacks.getSnacksname());
		s.setPrice(snacks.getPrice());
		return snacksRepo.save(s);
	}
	
	// Delete
	public void deleteSnacks(Integer id) {
		snacksRepo.deleteById(id);
	}
	
	// Find by snacks id
	public Snacks getBysnacksId(int id) {
		return snacksRepo.findById(id).get();
	}
}
