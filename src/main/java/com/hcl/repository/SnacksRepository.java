package com.hcl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.entity.Snacks;

@Repository
public interface SnacksRepository extends JpaRepository<Snacks,Integer> {

}
