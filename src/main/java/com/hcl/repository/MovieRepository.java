package com.hcl.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.entity.Movie;

@Repository
public interface MovieRepository extends JpaRepository<Movie,Integer> {

	public List<Movie> findBygenre(String genre);
	
	public List<Movie> findBylanguage(String language);
	
	public List<Movie> findByreleasetype(String releasetype);
	
	public List<Movie> findBymoviename(String moviename);
}

