package com.hcl.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;


import com.hcl.entity.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order,Integer> {
	
	public List<Order> findByuserid(Integer userid);
}