package com.hcl.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.entity.Movie;
import com.hcl.entity.Snacks;
import com.hcl.service.MovieService;
import com.hcl.service.SnacksService;

@RestController
@RequestMapping("/admin")
public class AdminController {
	
	
	@Autowired
	MovieService movieService;
	
	@Autowired 
	SnacksService snacksService;
	
	
	
	// Admin Login
	@PostMapping("/login")
	public String Login(@RequestBody Map<String, Object> payload,HttpSession session) {
		String email = "admin@gmail.com";
		String password = "admin";
		if (email.equals(payload.get("email")) && password.equals(payload.get("password"))) {
			session.setAttribute("adminid",1);
			return  " Admin Login Successfully";
		}else {
			return "Login Failed";
		}
	}
	
	// Admin Logout
	@GetMapping("/logout")
	public String logout(HttpSession session)
	{
		session.removeAttribute("adminid");
		session.invalidate();
		return "Admin Logout Successfully";
	}
	
	// Add Movie
	@PostMapping("/addMovie")
	public String addMovie(@RequestBody Map<String, Object> payload) {
		Movie m = new Movie();
		m.setMoviename(payload.get("moviename").toString());
		m.setGenre(payload.get("genre").toString());
		m.setLanguage(payload.get("language").toString());
		m.setReleasetype(payload.get("releasetype").toString());
		m.setPrice(payload.get("price").hashCode());
		movieService.createMovie(m);
		return "New Movie Added";
	}
	
	// UpdateMovie
	@PutMapping("/updateMovie/{id}")
	public String updateMovie(@PathVariable Integer id,@RequestBody Map<String, Object> payload) {
		Movie m = new Movie();
		m.setMoviename(payload.get("moviename").toString());
		m.setGenre(payload.get("genre").toString());
		m.setLanguage(payload.get("language").toString());
		m.setReleasetype(payload.get("releasetype").toString());
		m.setPrice(payload.get("price").hashCode());
		movieService.updateMovie(id, m);
		return "Movie updated";
	}
	
	// View All Movies
	@GetMapping("/viewAllMovie")
	public List<Movie> displayAllMovies() {
		return movieService.viewAllMovies();
	}
	
	// Delete Movie By Id
	@DeleteMapping("/deleteMovie/{id}")
	public String deleteMovie(@PathVariable Integer id) {
		movieService.deleteMovie(id);
		return "Movie Deleted";
	}
	
	// Add Snacks
	@PostMapping("/addSnacks")
	public String addSnacks(@RequestBody Map<String, Object> payload) {
		
		Snacks s = new Snacks();
		s.setSnacksname(payload.get("snacksname").toString());
		s.setPrice(payload.get("price").hashCode());
		snacksService.createSnacks(s);	
		return "New Snack Added";
	}
	
	// Update Snacks
	@PutMapping("/updateSnacks/{id}")
	public String updateSnacks(@PathVariable Integer id,@RequestBody Map<String, Object> payload) {
		Snacks s = new Snacks();
		s.setSnacksname(payload.get("snacksname").toString());
		s.setPrice(payload.get("price").hashCode());
		snacksService.updateSnacks(id, s);
		return "Snacks Updated";
	}
	
	// View Snacks
	@GetMapping("/viewSnacks")
	public List<Snacks> viewAllSnacks() {
		return snacksService.viewAllSnackss();
	}
	
	// Delete Snacks by Id
	@DeleteMapping("/deleteSnacks/{id}") 
	public String deleteSnacks(Integer id) {
		snacksService.deleteSnacks(id);
		return "Snacks Deleted";
	}
	
	
	

}
