package com.hcl.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.entity.Movie;
import com.hcl.entity.Order;
import com.hcl.entity.Snacks;
import com.hcl.entity.User;
import com.hcl.service.MovieService;
import com.hcl.service.OrderService;
import com.hcl.service.SnacksService;
import com.hcl.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	MovieService movieService;
	
	@Autowired 
	SnacksService snacksService;
	
	@Autowired
	OrderService orderService;
	
	@Autowired
	UserService userService;
	
	// User Register
	@PostMapping("/register") 
	public String Register(@RequestBody Map<String, Object> payload) {
		
		User u = new User();
		u.setEmail(payload.get("email").toString());
		u.setPassword(payload.get("password").toString());
		u.setUsername(payload.get("username").toString());
		userService.createUser(u);
		return  payload.get("username") + " User Registered Successfully";
	}
	
	// User Login
	@PostMapping("/login")
	public String Login(@RequestBody Map<String, Object> payload,HttpSession session) {
		boolean isLogged = userService.loginCheck(payload.get("email").toString(), payload.get("password").toString());
		if (isLogged) {
			session.setAttribute("email",payload.get("email").toString());
			return  " User Login Successfully";
		}else {
			return "Login Failed";
		}
	}
	
	// User Logout
	@GetMapping("/logout")
	public String logout(HttpSession session)
	{
		session.removeAttribute("email");
		session.invalidate();
		return "User Logout Successfully";
	}
	
	// View All Movies
	@GetMapping("/showAllMovie")
	public List<Movie> showAllMovies() {
		return movieService.viewAllMovies();
	}
	
	// View Snacks
	@GetMapping("/showSnacks")
	public List<Snacks> showAllSnacks() {
		return snacksService.viewAllSnackss();
	}
	
	// User Create Order
	@PostMapping("/newOrder")
	public String createOrder(@RequestBody Map<String, Object> payload) {
		Order o = new Order();
		o.setMovie(movieService.getById(payload.get("movieid").hashCode()));
		o.setSnacks(snacksService.getBysnacksId(payload.get("snacksid").hashCode()));
		o.setUserid(payload.get("userid").hashCode());
		orderService.createOrder(o);
		return "New Order made By User";
	}
	
	// view all Order
	@GetMapping("/viewAllOrder")
	public List<Order> displayAllOrder() {
		return orderService.ViewAllOrders();
	}
	
	// Delete order
	@DeleteMapping("/deleteOrder/{id}")
	public String deleteOrder(@PathVariable Integer id) {
		orderService.deleteOrder(id);
		return "Order is cancelled";	
	}
	
	// Find Movie By Genre
	@GetMapping("/getBygenre/{genre}")
	public List<Movie> viewBygenre(@PathVariable String genre) {
		return movieService.getBygenre(genre);
	}
	
	//find by language
	@GetMapping("/getBylanguage/{language}")
	public List<Movie> viewbylanguage(@PathVariable String language) {
		return movieService.getBylanguage(language);
	}
	
	//Find Movie by release type
	@GetMapping("/getByreleasetype/{releasetype}")
	public List<Movie> viewbyreleasetype(@PathVariable String releasetype) {
		return movieService.getByreleasetype(releasetype);
	}
	
	//Find Movie by Movie Name
	@GetMapping("/getBymoviename/{moviename}")
	public List<Movie> viewBymoviename(@PathVariable String moviename) {
		return movieService.getBymoviename(moviename);
	}
	
	//To Get Order Details
	@GetMapping("/viewByuserid/{userid}")
	public List<Order> viewByuserid(@PathVariable Integer userid) {
		return orderService.getByuserid(userid);
	}
}
