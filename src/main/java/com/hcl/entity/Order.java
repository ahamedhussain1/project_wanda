package com.hcl.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="orders")
public class Order {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int orderid;
	
	
	@Column(name="userid")
	private int userid;
	
	@OneToOne()
    @JoinColumn(name="movieid")
    private Movie movie;
	
	@OneToOne()
    @JoinColumn(name="snacksid")
    private Snacks snacks;

	
	public Order() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Order(int orderid, int userid, Movie movie, Snacks snacks) {
		super();
		this.orderid = orderid;
		this.userid = userid;
		this.movie = movie;
		this.snacks = snacks;
	}


	@Override
	public String toString() {
		return "Order [orderid=" + orderid + ", userid=" + userid + ", movie=" + movie + ", snacks=" + snacks + "]";
	}


	public int getOrderid() {
		return orderid;
	}


	public void setOrderid(int orderid) {
		this.orderid = orderid;
	}


	public int getUserid() {
		return userid;
	}


	public void setUserid(int userid) {
		this.userid = userid;
	}


	public Movie getMovie() {
		return movie;
	}


	public void setMovie(Movie movie) {
		this.movie = movie;
	}


	public Snacks getSnacks() {
		return snacks;
	}


	public void setSnacks(Snacks snacks) {
		this.snacks = snacks;
	}

	
	

}
