package com.hcl.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="snacks_table")
public class Snacks {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int snacksid;
	
	@Column(name="snacksname")
	private String snacksname;
	@Column(name="price")
	private int price;
	
	@OneToOne(fetch=FetchType.LAZY, mappedBy="snacks")
    private Order order;
	
	
	public Snacks() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Snacks(int snacksid, String snacksname, int price) {
		super();
		this.snacksid = snacksid;
		this.snacksname = snacksname;
		this.price = price;
	}

	@Override
	public String toString() {
		return "Snacks [snacksid=" + snacksid + ", snacksname=" + snacksname + ", price=" + price + "]";
	}

	public int getSnacksid() {
		return snacksid;
	}

	public void setSnacksid(int snacksid) {
		this.snacksid = snacksid;
	}

	public String getSnacksname() {
		return snacksname;
	}

	public void setSnacksname(String snacksname) {
		this.snacksname = snacksname;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
	
	

}
