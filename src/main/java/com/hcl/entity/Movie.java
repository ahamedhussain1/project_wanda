package com.hcl.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="movie_table")
public class Movie {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int movieid;
	
	@Column(name="moviename")
	private String moviename;
	@Column(name="genre")
	private String genre;
	@Column(name="language")
	private String language;
	@Column(name="releasetype")
	private String releasetype;
	@Column(name="price")
	private int price;
	
	
	@OneToOne(fetch=FetchType.LAZY, mappedBy="movie")
    private Order order;
	
	
	public Movie() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Movie(int movieid, String moviename, String genre, String language, String releasetype, int price) {
		super();
		this.movieid = movieid;
		this.moviename = moviename;
		this.genre = genre;
		this.language = language;
		this.releasetype = releasetype;
		this.price = price;
	}


	@Override
	public String toString() {
		return "Movie [movieid=" + movieid + ", moviename=" + moviename + ", genre=" + genre + ", language=" + language
				+ ", releasetype=" + releasetype + ", price=" + price + "]";
	}


	public int getMovieid() {
		return movieid;
	}


	public void setMovieid(int movieid) {
		this.movieid = movieid;
	}


	public String getMoviename() {
		return moviename;
	}


	public void setMoviename(String moviename) {
		this.moviename = moviename;
	}


	public String getGenre() {
		return genre;
	}


	public void setGenre(String genre) {
		this.genre = genre;
	}


	public String getLanguage() {
		return language;
	}


	public void setLanguage(String language) {
		this.language = language;
	}


	public String getReleasetype() {
		return releasetype;
	}


	public void setReleasetype(String releasetype) {
		this.releasetype = releasetype;
	}


	public int getPrice() {
		return price;
	}


	public void setPrice(int price) {
		this.price = price;
	}
	
	
	
	

}
